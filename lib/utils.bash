#!/usr/bin/env bash

set -euo pipefail

TOOL_NAME="clickhouse"
TOOL_TEST="clickhouse"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_all_versions() {
  curl -s https://packages.clickhouse.com/tgz/stable/ | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | sort -V -r | uniq
}

download_release() {
  local version
  version="$1"
  echo "* Downloading $TOOL_NAME release $version .."

  cd "$CARI_DOWNLOAD_PATH/"

  rm -f "clickhouse-client_${version}_amd64.deb" &
  rm -f "clickhouse-common-static_${version}_amd64.deb" &
  rm -f "clickhouse-server_${version}_amd64.deb" &

  url="https://packages.clickhouse.com/deb/pool/main/c/clickhouse/clickhouse-client_${version}_amd64.deb"
  echo "* Downloading 1 of 3 .."
  curl -fO $url || fail "Could not download $url"

  url="https://packages.clickhouse.com/deb/pool/main/c/clickhouse/clickhouse-common-static_${version}_amd64.deb"
  echo "* Downloading 2 of 3 .."
  curl -fO $url || fail "Could not download $url"

  url="https://packages.clickhouse.com/deb/pool/main/c/clickhouse/clickhouse-server_${version}_amd64.deb"
  echo "* Downloading 3 of 3 .."
  curl -fO $url || fail "Could not download $url"

  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    sudo dpkg -i $CARI_DOWNLOAD_PATH/clickhouse*.deb

    mkdir -p "$install_path/bin"
    touch "$install_path/bin/clickhouse"
    echo "#!/bin/bash" >> $install_path/bin/clickhouse
    echo "if [ \"\$1\" == \"client\" ]; then" >> $install_path/bin/clickhouse
    echo "  /usr/bin/clickhouse client --password" >> $install_path/bin/clickhouse
    echo "  exit 0" >> $install_path/bin/clickhouse
    echo "fi" >> $install_path/bin/clickhouse
    echo "sudo /usr/bin/clickhouse \$1" >> $install_path/bin/clickhouse
    chmod a+x "$install_path/bin/clickhouse"

    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  local version
  version="$1"
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
